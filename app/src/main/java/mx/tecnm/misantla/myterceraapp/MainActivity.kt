package mx.tecnm.misantla.myterceraapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_Enviar.setOnClickListener {

            val nombre = edt_Nombre.text.toString()
            val apellido = edt_Apellido.text.toString()
            val email = edt_email.text.toString()
            val password = edt_password.text.toString()

            if(nombre.isEmpty()){
                Toast.makeText(this,"Debe ingresar un  nombre",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(apellido.isEmpty()){
                Toast.makeText(this,"Debe ingresar un  apellido",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(email.isEmpty()){
                Toast.makeText(this,"Debe ingresar un email",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(password.isEmpty()){
                Toast.makeText(this,"Debe ingresar un password",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            val bundle = Bundle()
            bundle.apply {
                putString("dato1",nombre)
                putString("dato2",apellido)
                putString("dato3",email)
                putString("dato4",password)
            }

            val intent = Intent(this,MainActivity2::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}