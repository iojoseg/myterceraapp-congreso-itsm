package mx.tecnm.misantla.myterceraapp

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val bundle : Bundle? = intent.extras

        bundle?.let {
            val nombre = it.getString("dato1")
            val apellido = it.getString("dato2")
            val email = it.getString("dato3")
            val password = it.getString("dato4")

            txtnombre.text = "Nombre: $nombre"
            txtapellido.text = "Apellido: $apellido"
            txtemail.text = "Email: $email"
            txtpassword.text = "Password: $password"
        }


        val comprobar_permiso = ContextCompat.checkSelfPermission(
            this,android.Manifest.permission.CALL_PHONE
        )

        if (comprobar_permiso != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                this, arrayOf<String>(android.Manifest.permission.CALL_PHONE),255)
        }

        imageButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: 2351047885"))
            startActivity(intent)
        }
    }
}